# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

import sweat.models as m

# Register your models here.

#Address
class AddressAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.Address, AddressAdmin)


#Profile
class ProfileAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.Profile, ProfileAdmin)


#Organization
class OrganizationAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.Organization, OrganizationAdmin)




#Answer
class AnswerAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.Answer, AnswerAdmin)

#Message
class MessageAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.Message, MessageAdmin)

#MessageOption
class MessageOptionAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.MessageOption, MessageOptionAdmin)

#Script
class ScriptAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.Script, ScriptAdmin)

#ScriptOption
class ScriptOptionAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.ScriptOption, ScriptOptionAdmin)

#Scripts
class ScriptsAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.Scripts, ScriptsAdmin)





#Participant
class ParticipantAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.Participant, ParticipantAdmin)

#InterestedParty
class InterestedPartyAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.InterestedParty, InterestedPartyAdmin)

#Communication
class CommunicationAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.Communication, CommunicationAdmin)



#TrainingDay
class TrainingDayAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.TrainingDay, TrainingDayAdmin)

#TrainingPlanDetail
class TrainingPlanDetailAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.TrainingPlanDetail, TrainingPlanDetailAdmin)

#TrainingPlanWeek
class TrainingPlanWeekAdmin(admin.ModelAdmin):
    exclude=['uuid']

admin.site.register(m.TrainingPlanWeek, TrainingPlanWeekAdmin)
