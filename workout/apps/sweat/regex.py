
#from foundation_core import regex as rgx


street = '^[\s\S]{2,50}$'
city = '^[a-zA-Z .]+$'
state = '^(Alabama|Alaska|Arizona|Arkansas|California|Colorado|Connecticut|Delaware|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Mississippi|Missouri|Montana|Nebraska|Nevada|New\sHampshire|New\sJersey|New\sMexico|New\sYork|North\sCarolina|North\sDakota|Ohio|Oklahoma|Oregon|Pennsylvania|Rhode\sIsland|South\sCarolina|South\sDakota|Tennessee|Texas|Utah|Vermont|Virginia|Washington|West\sVirginia|Wisconsin|Wyoming)|([Aa][LKSZRAEPlkszraep]|[Cc][AOTaot]|[Dd][ECec]|[Ff][LMlm]|[Gg][AUau]|[Hh][Ii]|[Ii][ADLNadln]|[Kk][SYsy]|[Ll][Aa]|[Mm][ADEHINOPSTadehinopst]|[Nn][CDEHJMVYcdehjmvy]|[Oo][HKRhkr]|[Pp][ARWarw]|[Rr][Ii]|[Ss][CDcd]|[Tt][NXnx]|[Uu][Tt]|[Vv][AITait]|[Ww][AIVYaivy])$'
zipcode = '^([0-9]{5}(?:-[0-9]{4})?)+$'
zipcode_anywhere = '([0-9]{5}(?:-[0-9]{4})?)'

role = '^[a-zA-Z ]+$'


filepath = '^[\w\/\-]*$'

slugfield = '^[\w\.-]+$'


alpha = '^[a-zA-z]*$'
digit = '^[\d]*$'
decimal = '^[\d.]*$'

generic_id = '^[a-zA-z0-9\ -.]*$'

generic_license = '^[a-zA-z\d-]*$'




#000000000000000000
standard_object_name = '^[\w\d\S\ ]*$'
standard_object_description = '^[\w\d\S\ ]*$'
