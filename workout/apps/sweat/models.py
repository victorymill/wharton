# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import uuid

from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core import validators as dcv
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from sweat import regex as rgx


class Address(models.Model):
    """
    Address Model. This model is used to record U.S. Addresses.
    Simple addresses are recorded and sent for processing to
    incorperate geocoding information.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    street_01 = models.CharField(
        max_length=50,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.street,
                message='Enter Valid Street Address',
                code='invalid_street_01'
            ),
        ],
    )
    street_02 = models.CharField(
        blank=True,
        null=True,
        max_length=50,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.street,
                message='Enter Valid Additional Street Address',
                code='invalid_street_02'
            ),
        ],
    )
    city = models.CharField(
        max_length=50,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.city,
                message='Enter Valid City',
                code='invalid_city'
            ),
        ],
    )
    state = models.CharField(
        max_length=20,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.state,
                message='Enter Valid State',
                code='invalid_state'
            ),
        ],
    )
    zipcode = models.CharField(
        max_length=5,
        validators=[
            dcv.RegexValidator(
                regex=rgx.zipcode,
                message='Enter Valid Zipcode',
                code='invalid_zipcode'
            ),
        ],
    )
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    class Meta:
        app_label = 'sweat'
        verbose_name = u'Address'
        verbose_name_plural = u'Addresses'

    def __unicode__(self):
        return '{0}'.format(self.uuid)

    def get_address_as_line(self):
        street = self.street_01
        if self.street_02 != '' and self.street_02 != None:
            street = '{0}, {1}'.format(
                street,
                self.street_02
            )
        city_state_zipcode = '{0}, {1}, {2} {3}'.format(
            street,
            self.city,
            self.state,
            self.zipcode,
        )
        return city_state_zipcode






class Profile(models.Model):
    """
    Profile Model. This model is used to record all
    data that should be associated with a user's account.
    This is the standard class that other records are linked
    with.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User)
    phone = PhoneNumberField()
    address = models.OneToOneField(Address, related_name="profile")

    class Meta:
        app_label = 'sweat'
        verbose_name = u'Profile'
        verbose_name_plural = u'Profiles'

    def __unicode__(self):
        return '{0}'.format(self.uuid)


class Organization(models.Model):
    """
    Organization model. This model is used to manage an
    all aspects of an organization's service. Contact info
    for the organization is maintained here, along with
    billable offerings, invoices, credits and Cloud Services.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(
        max_length=50,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
        ],
    )
    phone = PhoneNumberField()
    url = models.URLField()
    slug = models.SlugField(
        unique=True,
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
    )
    active = models.BooleanField(default=True)
    address = models.OneToOneField(Address, related_name="organization")

    class Meta:
        app_label = 'sweat'
        verbose_name = u'Organization'
        verbose_name_plural = u'Organizations'

    def __unicode__(self):
        return '{0}'.format(self.uuid)






COMMUNICATION_TYPE_CHOICES = (
    ('day_pre', 'Pre Workout'),
    ('day_post', 'Post Workout'),
    ('program_pre', 'Pre Program'),
    ('program_post', 'Post Program'),
)


SELECTION_CHOICES = (
    ('manual', 'Manual'),
    ('algorithm', 'Algorithm'),
)


RELATIONSHIP_CHOICES = (
    ('friend', 'friend'),
    ('family', 'family'),
    ('significant_other', 'significant_other'),
    ('stranger', 'stranger'),
    ('mentor', 'mentor'),
    ('colleague', 'colleague'),
)


WEEKDAY_CHOICES = (
    ('mon', 'Monday'),
    ('tues', 'Tuesday'),
    ('wed', 'Wednesday'),
    ('thur', 'Thursday'),
    ('fri', 'Friday'),
    ('sat', 'Saturday'),
    ('sun', 'Sunday'),
)


class Message(models.Model):
    """
    Message model. This model is used to define what the actual
    SMS content will be. If content_literal, the content is texted
    exactly how it is presented. Otherwise, the content may contain
    variables that need to be processed (like the users name). You
    define both content for participants and interested_parties so
    they both get relevent messages for the defined event.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    communication_type = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=COMMUNICATION_TYPE_CHOICES,
        default='day_pre',
    )
    content_literal = models.BooleanField(default=True)
    content_participant = models.CharField(
        max_length=160,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
        ],
    )
    content_interested_party = models.CharField(
        max_length=160,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
        ],
    )

    class Meta:
        app_label = 'sweat'
        verbose_name = u'Message'
        verbose_name_plural = u'Messages'

    def __unicode__(self):
        return '{0}'.format(self.uuid)






class Answer(models.Model):
    """
    Answer model.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    participant = models.ForeignKey('Participant', related_name="answers")
    content = models.CharField(
        max_length=160,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
        ],
    )

    class Meta:
        app_label = 'sweat'
        verbose_name = u'Answer'
        verbose_name_plural = u'Answers'

    def __unicode__(self):
        return '{0}'.format(self.uuid)




class MessageOption(models.Model):
    """
    MessageOption model. This model allows a researcher to
    define a weight for a message. It is used by Script Models.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    weight = models.CharField(
        max_length=7,
        default=10.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Weight for Message Option',
                code='invalid_weight'
            ),
        ],
    )
    message = models.ForeignKey(Message, related_name="message_options")


    class Meta:
        app_label = 'sweat'
        verbose_name = u'MessageOption'
        verbose_name_plural = u'MessageOptions'

    def __unicode__(self):
        return '{0}'.format(self.uuid)





class Script(models.Model):
    """
    Script model. This model is used to create a batch
    of messages in case the researcher wants to A/B test messages.
    The idea is that an algorith can select a message randomly,
    while considering the defined weight of the message option.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(
        max_length=50,
        unique=True,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
        ],
    )
    description = models.TextField(
        validators=[
            dcv.MinLengthValidator(5, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.standard_object_description,
                message='Enter Valid Description',
                code='invalid_description'
            ),
        ],
    )
    communication_type = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=COMMUNICATION_TYPE_CHOICES,
        default='day_pre',
    )

    message_options = models.ManyToManyField(MessageOption, blank=True, related_name='scripts')


    class Meta:
        app_label = 'sweat'
        verbose_name = u'Script'
        verbose_name_plural = u'Scripts'

    def __unicode__(self):
        return '{0}'.format(self.uuid)




class ScriptOption(models.Model):
    """
    ScriptOption model. This model allows a researcher to
    define a weight for a Script. It is used by Scripts Models.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    weight = models.CharField(
        max_length=7,
        default=10.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Weight for Message Option',
                code='invalid_weight'
            ),
        ],
    )
    script = models.ForeignKey(Script, related_name="script_options")

    class Meta:
        app_label = 'sweat'
        verbose_name = u'ScriptOption'
        verbose_name_plural = u'ScriptOptions'

    def __unicode__(self):
        return '{0}'.format(self.uuid)



class Scripts(models.Model):
    """
    Scripts model. This model is used to define batches of
    Script models. The Script models are weighted and can
    be selected randomly (while considering the defined weight).
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(
        max_length=50,
        unique=True,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
        ],
    )
    description = models.TextField(
        validators=[
            dcv.MinLengthValidator(5, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.standard_object_description,
                message='Enter Valid Description',
                code='invalid_description'
            ),
        ],
    )
    communication_type = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=COMMUNICATION_TYPE_CHOICES,
        default='day_pre',
    )

    script_options = models.ManyToManyField(ScriptOption, blank=True, related_name='scripts')


    class Meta:
        app_label = 'sweat'
        verbose_name = u'Scripts'
        verbose_name_plural = u'Scriptss'

    def __unicode__(self):
        return '{0}'.format(self.uuid)


    #script = models.ForeignKey(Script, related_name="script_options")


class Participant(models.Model):
    """
    Participant model. This model is used to define a test subject.
    Each person gets a Profile (& Address & User Account). Additionally,
    an Organization may be defined. Model maintains stats on program
    progress in order to decrease DB calls to gen stats. Also, because
    researchers are testing effects of messaging on behavior, I added
    fields for both pre & post workout (and pre & post entire program)
    even though that wasn't specified yet. Goal is to provide researchers
    flexibility to test & record any behavior they wish.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=False)
    organization = models.ForeignKey(Organization, related_name="participant")
    profile = models.OneToOneField(Profile)

    program_completed = models.BooleanField(default=False)
    program_ended_due_to_disability = models.BooleanField(default=False)

    n_program_day = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Program Day #',
                code='invalid_n_program_day'
            ),
        ],
    )

    n_training_day = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Training Day #',
                code='invalid_n_training_day'
            ),
        ],
    )


    script_day_pre = models.ForeignKey(Scripts, related_name="days_pre")
    script_day_post = models.ForeignKey(Scripts, related_name="days_post")
    script_program_pre = models.ForeignKey(Scripts, related_name="programs_pre")
    script_program_post = models.ForeignKey(Scripts, related_name="programs_post")

    selection_method_script_day_pre = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=SELECTION_CHOICES,
        default='algorithm',
    )
    selection_method_script_day_post = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=SELECTION_CHOICES,
        default='algorithm',
    )
    selection_method_script_program_pre = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=SELECTION_CHOICES,
        default='algorithm',
    )
    selection_method_script_program_post = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=SELECTION_CHOICES,
        default='algorithm',
    )



    #reward_plan
    selection_method_reward_plan = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=SELECTION_CHOICES,
        default='algorithm',
    )

    reward_balance = models.CharField(
        max_length=7,
        default=10.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Reward Balance',
                code='invalid_reward_balance'
            ),
        ],
    )
    timestamp_reward_balance = models.DateTimeField()
    reward_redeemed = models.BooleanField(default=False)
    timestamp_reward_redeemed = models.DateTimeField()

    class Meta:
        app_label = 'sweat'
        verbose_name = u'Participant'
        verbose_name_plural = u'Participants'

    def __unicode__(self):
        return '{0}'.format(self.uuid)





class InterestedParty(models.Model):
    """
    InterestedParty model. This model is used to define the people
    the Participant wants to broadcast program updates. These users
    will not be able to log in, but will be able to opt-out via SMS interface.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    participant = models.ForeignKey(Participant, related_name="interested_parties")
    timestamp = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=False)
    opted_out = models.BooleanField(default=False)
    timestamp_inactive = models.DateTimeField()

    n_program_day_added = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Program Day #',
                code='invalid_n_program_day_added'
            ),
        ],
    )


    name = models.CharField(
        max_length=50,
        validators=[
            dcv.MinLengthValidator(2, message='A Longer Value is Required'),
        ],
    )
    relationship = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=RELATIONSHIP_CHOICES,
        default='friend',
    )


    phone = PhoneNumberField()



    n_program_day = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Program Day #',
                code='invalid_n_program_day'
            ),
        ],
    )
    n_communications = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Communications Count (#N)',
                code='invalid_n_communications'
            ),
        ],
    )

    class Meta:
        app_label = 'sweat'
        verbose_name = u'InterestedParty'
        verbose_name_plural = u'InterestedParties'

    def __unicode__(self):
        return '{0}'.format(self.uuid)



class Communication(models.Model):
    """
    Communication model. This model is used to record an SMS
    transaction. The record holds the message sent (for historical
    record) and links it to human users and training days.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    participant = models.ForeignKey(Participant, related_name="communications")
    interested_party = models.ForeignKey(InterestedParty, related_name="communications", blank=True, null=True)
    communication_type = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=COMMUNICATION_TYPE_CHOICES,
        default='day_pre',
    )
    message = models.ForeignKey(Message, related_name="communications")
    #twilio info

    training_days = models.ManyToManyField("TrainingDay", related_name="communications", blank=True)

    class Meta:
        app_label = 'sweat'
        verbose_name = u'Communication'
        verbose_name_plural = u'Communications'

    def __unicode__(self):
        return '{0}'.format(self.uuid)



class TrainingDay(models.Model):
    """
    TrainingDay model. This model is used to define a day
    that the test subject intends to exercise. Communications
    to all users are linked & participant progress is recorded
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    participant = models.ForeignKey(Participant, related_name="training_days")

    n_program_day = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Program Day #',
                code='invalid_n_program_day'
            ),
        ],
    )
    n_training_day = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Training Day #',
                code='invalid_n_training_day'
            ),
        ],
    )

    weekday = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=WEEKDAY_CHOICES,
        default='mon',
    )

    workout_occurred = models.BooleanField(default=True)

    n_communications_pre = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Pre Communications Count (#N)',
                code='invalid_n_communications_pre'
            ),
        ],
    )

    n_communications_post = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Post Communications Count (#N)',
                code='invalid_n_communications_post'
            ),
        ],
    )

    credit_amount_awarded = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Credit Amount Awarded (#)',
                code='invalid_credit_amount_awarded'
            ),
        ],
    )

    class Meta:
        app_label = 'sweat'
        verbose_name = u'TrainingDay'
        verbose_name_plural = u'TrainingDays'

    def __unicode__(self):
        return '{0}'.format(self.uuid)


class TrainingPlanDetail(models.Model):
    """
    TrainingPlanDetail model. This model is used by the user,
    to define their workout schedule. The define workout time,
    and indicate how early they want to receive their reminder SMS.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    participant = models.ForeignKey(Participant, related_name="training_plan_details")
    timestamp_workout = models.DateTimeField(auto_now_add=True)
    weekday = models.SlugField(
        validators=[
            dcv.MinLengthValidator(3, message='A Longer Value is Required'),
        ],
        choices=WEEKDAY_CHOICES,
        default='mon',
    )
    active = models.BooleanField(default=True)
    timestamp_notify = models.DateTimeField()
    timestamp_notify_lead = models.CharField(
        max_length=7,
        default=0.0,
        validators=[
            dcv.MinLengthValidator(1, message='A Longer Value is Required'),
            dcv.RegexValidator(
                regex=rgx.decimal,
                message='Enter Valid Notification Lead Hours (#N)',
                code='timestamp_notify_lead'
            ),
        ],
    )
    class Meta:
        app_label = 'sweat'
        verbose_name = u'TrainingPlanDetail'
        verbose_name_plural = u'TrainingPlanDetails'

    def __unicode__(self):
        return '{0}'.format(self.uuid)


class TrainingPlanWeek(models.Model):
    """
    TrainingPlanWeek model. This model is used to bundle
    all of the Participants individual training days. This
    approach was chosen over simply using a performing a lookup
    of all TrainingPlanDetail and filtering by weekday to access
    correct single TrainingPlanDetail.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    participant = models.ForeignKey(Participant, related_name="training_plans")
    timestamp_updated = models.DateTimeField()
    active = models.BooleanField(default=True)

    monday = models.ForeignKey(TrainingPlanDetail, related_name="training_day_monday_week")
    tuesday = models.ForeignKey(TrainingPlanDetail, related_name="training_day_tuesday_week")
    wednesday = models.ForeignKey(TrainingPlanDetail, related_name="training_day_wednesday_week")
    thursday = models.ForeignKey(TrainingPlanDetail, related_name="training_day_thursday_week")
    friday = models.ForeignKey(TrainingPlanDetail, related_name="training_day_friday_week")
    saturday = models.ForeignKey(TrainingPlanDetail, related_name="training_day_saturday_week")
    sunday = models.ForeignKey(TrainingPlanDetail, related_name="training_day_sunday_week")

    class Meta:
        app_label = 'sweat'
        verbose_name = u'TrainingPlanWeek'
        verbose_name_plural = u'TrainingPlanWeeks'

    def __unicode__(self):
        return '{0}'.format(self.uuid)



User.profile = property(lambda u: Profile.objects.get_or_create(user=u)[0])
